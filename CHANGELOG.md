CHANGELOG
=========

1.0.1 (2013-10-08)

* fixed bug in passing session to Guard

1.0.0 (2013-10-03)
------------------

* initial release
